# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
   str.each_char.reject {|ch| ch != ch.upcase}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
   mid=str.length/2
   str[mid-1..mid]
else
    mid=str.length/2
    str[mid]
end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = ["a", "e", "i", "o", "u"]
  count_vowels=0
  str.each_char do |ch|
    if vowels.include?(ch.downcase)
      count_vowels += 1
    end
  end
  count_vowels

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product=num
  until num == 1
    num -= 1
    product *= num
  end
  product

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joinstring = ""
  arr.each_index do |str|
    joinstring += arr[str].to_s
    joinstring += separator if str != arr.length - 1
  end
  joinstring

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
 chars=str.chars
 result_string=""
 chars.each_index do |index|
 if index.even?
     result_string += chars[index].downcase
  else
     result_string += chars[index].upcase
 end
 end
result_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
   result_array=[]
   new_string = str.split
   new_string.each do |ch|
   if ch.length > 4
       ch = ch.reverse
   end
       result_array << ch
   end
       result_array.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz = []
 (1..n).each do |fiz|
   if fiz % 3 == 0 && fiz % 5 == 0
     fizzbuzz << "fizzbuzz"
   elsif fiz % 5 == 0
     fizzbuzz << "buzz"
   elsif  fiz % 3 == 0
      fizzbuzz << "fizz"
   else
     fizzbuzz << fiz
   end

 end

 fizzbuzz


end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []

  arr.each do |idx|
    new_arr << idx
  end

  new_arr.reverse


end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
   (2..num/2).each do |idx|
    if num % idx == 0
      return false
    end
  end
      true

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sort_array = []
  (1..num).each do |idx|
    if num % idx == 0
      sort_array << idx
    end
  end
  sort_array

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  sort_array = []
  factors(num).each do |idx|
     sort_array << idx  if prime?(idx)
   end
  sort_array

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
 odd= []
 even= []
 arr.each do |idx|
  odd << idx if idx.even?
  even << idx if idx.odd?
 end

 if even.length > 1
  return odd[0]
 else
  return even [0]
 end
end
